﻿import { Component, Injector } from '@angular/core';
import {
    PagedListingComponentBase,
    PagedRequestDto,
} from '@shared/common/paged-listing-component-base';
import {
    ArticleListDto,
    ArticleServiceProxy,
    PagedResultDtoOfArticleListDto
} from '@shared/service-proxies/service-proxies';
import { CreateOrEditArticleModalComponent } from './create-or-edit-article-modal.component';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd';

@Component({
    selector: 'app-articles',
    templateUrl: './article.component.html',
    styles: [],
})
export class ArticleComponent extends PagedListingComponentBase<ArticleListDto> {
    advancedFiltersVisible = false;
    selectedPermission = '';
    editFormVisible = false;

    constructor(injector: Injector, private _articleService: ArticleServiceProxy, private drawerService: NzDrawerService) {
        super(injector);
    }

    protected fetchDataList(
        request: PagedRequestDto,
        pageNumber: number,
        finishedCallback: () => void,
    ): void {
        this._articleService.getArticles(
            this.filterText,
            request.sorting,
            request.maxResultCount,
            request.skipCount,
        )
        .pipe(finalize(finishedCallback))
        .subscribe((result: PagedResultDtoOfArticleListDto) => {
            this.dataList = result.items;
            this.showPaging(result);
        });
    }

    protected delete(entity: ArticleListDto): void {
        this._articleService.delete(entity.id).subscribe(() => {
            this.refresh();
            this.notify.success(this.l('SuccessfullyDeleted'));
        });
    }

    createOrEdit(id?: number): void {
        const drawerRef = this.drawerService.create<CreateOrEditArticleModalComponent, { articleId: number }, string>({
            nzBodyStyle: { height: 'calc(100% - 55px)', overflow: 'auto', 'padding-bottom': '53px' },
            nzMaskClosable: false,
            nzWidth: 720,
            nzTitle: id === undefined ? '新增文章' : '编辑文章',
            nzContent: CreateOrEditArticleModalComponent,
            nzContentParams: {
                articleId: id
            }
        });

        drawerRef.afterClose.subscribe(data => {
            if (parseInt(data) > 0) {
                this.refresh();
            }
          });
    }

    batchDelete(): void {
        this.message.warn('method not implement!');
        // const selectCount = this.selectedDataItems.length;
        // if (selectCount <= 0) {
        //     abp.message.warn(this.l('SelectAnItem'));

        //     return;
        // }
        // this.message.confirm(
        //     this.l('<b class="text-red">{0}</b> items will be removed.', selectCount),
        //     this.l('AreYouSure'),
        //     res => {
        //         if (res) {
        //             let deletedIds = _.map(this.selectedDataItems, (item) => new EntityDto({ id: item.id }));
        //             this._articleService.batchDeleteArticles(deletedIds).subscribe(() => {
        //                 this.refresh();
        //                 this.notify.success(this.l('SuccessfullyDeleted'));
        //             });
        //         }
        //     },
        // );
    }
}
